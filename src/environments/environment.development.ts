export const environment = {
  baseUrl: 'http://localhost:3000/',
  endpointUser: 'users',
  endpointProduct: 'products',
  index: '?_page=',
  size: '&_per_page=',
};
