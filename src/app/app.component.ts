import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  // template: '<div>Bonjour</div>',
  styleUrl: './app.component.css',
})
export class AppComponent {}
