import { Component } from '@angular/core';
import { ApiBackendService } from '../services/api-backend.service';
import { environment } from '../../environments/environment.development';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
})
export class LoginComponent {
  constructor(
    private apiBackendService: ApiBackendService,
    private router: Router
  ) {}
  connexion(myForm: any) {
    // appel backend
    this.apiBackendService
      .getOne(environment.endpointUser, myForm.value.userName)
      .subscribe({
        next: (value) => {
          // navigation
          localStorage.setItem('role', value.login);
          // if (value.login == 'admin') {
          this.apiBackendService.setUserRole(value.login);
          this.router.navigateByUrl('admin');
          // } else {
          // }
        },
        error: (err) => {
          console.log(err);
          this.router.navigateByUrl('not-found');
        },
        complete() {},
      });
  }
}
