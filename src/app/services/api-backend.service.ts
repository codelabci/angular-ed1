import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.development';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiBackendService {
  constructor(private httpClient: HttpClient) {}

  public userRole: any;

  setUserRole(role: any) {
    this.userRole = role;
  }

  getAll(endpoint: any): Observable<any> {
    return this.httpClient.get(environment.baseUrl + endpoint);
  }
  getOne(endpoint: any, id: any): Observable<any> {
    return this.httpClient.get(environment.baseUrl + endpoint + '/' + id);
  }
  getWithPaginate(endpoint: any, index: any, size: any) {
    return this.httpClient.get(
      environment.baseUrl +
        endpoint +
        environment.index +
        index +
        environment.size +
        size
    );
  }
  postOne(endpoint: any, data: any): Observable<any> {
    return this.httpClient.post(environment.baseUrl + endpoint, data);
  }
  putOne(endpoint: any, id: any, data: any) {
    return this.httpClient.put(environment.baseUrl + endpoint + '/' + id, data);
  }
}
