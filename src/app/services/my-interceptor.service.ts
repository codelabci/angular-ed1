import {
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class MyInterceptorService implements HttpInterceptor {
  constructor() {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // const headers = new HttpHeaders().append('Authorization', `Bearer MonToke`);
    // const modifiedReq = req.clone();

    const modifiedReq = req.clone({
      setHeaders: {
        Authorization: `Bearer kadion`,
      },
    });

    return next.handle(modifiedReq);
  }
}
