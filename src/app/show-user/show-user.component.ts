import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiBackendService } from '../services/api-backend.service';
import { environment } from '../../environments/environment.development';

@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrl: './show-user.component.css',
})
export class ShowUserComponent implements OnInit {
  constructor(
    private activatedRoute: ActivatedRoute,
    private apiBackend: ApiBackendService
  ) {}

  @Input() userTransmit: any;
  currentUser: any;
  currentId: any;
  ngOnInit(): void {
    this.currentId = this.activatedRoute.snapshot.params['id'];
    if (this.currentId) {
      // init form with user value
      this.apiBackend
        .getOne(environment.endpointUser, this.currentId)
        .subscribe({
          next: (value) => (this.currentUser = value),
          error: (err) => console.log(err),
          complete() {
            console.log('complete');
          },
        });
    } else {
      this.currentUser = this.userTransmit;
    }
  }
}
