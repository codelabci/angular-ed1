import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../environments/environment.development';
import { ApiBackendService } from '../services/api-backend.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrl: './user.component.css',
})
export class UserComponent implements OnInit {
  myReactiveForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private httpClient: HttpClient,
    private activateRoute: ActivatedRoute,
    private apiBackend: ApiBackendService
  ) {}

  currentId: any;
  actionLibelle = 'Enregister';
  // currentUser: any;
  ngOnInit(): void {
    this.myReactiveForm = this.formBuilder.group({
      nom: this.formBuilder.control('', [Validators.required]),
      login: this.formBuilder.control(''),
      number: this.formBuilder.control(''),
      email: this.formBuilder.control(''),
    });
    this.currentId = this.activateRoute.snapshot.params['id'];
    if (this.currentId) {
      this.actionLibelle = 'Modifier';
      // init form with user value
      this.apiBackend
        .getOne(environment.endpointUser, this.currentId)
        .subscribe({
          next: (value) => this.intitForm(value),
          error: (err) => console.log(err),
          complete() {
            console.log('complete');
          },
        });
    } else {
    }
  }

  intitForm(value: any) {
    this.myReactiveForm.controls['login'].setValue(value.login);
    this.myReactiveForm.controls['nom'].setValue(value.nom);
    this.myReactiveForm.controls['email'].setValue(value.email);
    this.myReactiveForm.controls['number'].setValue(value.number);
  }
  onReactiveSubmit() {
    console.log('onReactiveSubmit ::: ', this.myReactiveForm.value);

    if (this.currentId) {
      this.apiBackend
        .putOne(
          environment.endpointUser,
          this.currentId,
          this.myReactiveForm.value
        )
        .subscribe();
    } else {
      this.apiBackend
        .postOne(environment.endpointUser, this.myReactiveForm.value)
        .subscribe();
    }

    // this.httpClient
    //   .post('http://localhost:3000/users', this.myReactiveForm.value)
    //   .subscribe();
  }
  isDisabled: boolean = false;
  onSubmit(myForm: NgForm) {
    console.log('ghejzefioel');
    this.isDisabled = true;
    console.log('onSubmit ::: ', myForm.valid);
    // apres retour de l'api on fait this.isDisabled = false;
  }
}
