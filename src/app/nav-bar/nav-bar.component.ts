import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiBackendService } from '../services/api-backend.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrl: './nav-bar.component.css',
})
export class NavBarComponent implements OnInit {
  constructor(
    private router: Router,
    public apiBackendService: ApiBackendService
  ) {}
  currentRole: any;
  ngOnInit(): void {
    this.currentRole = localStorage.getItem('role');
  }
  testBinding() {
    console.log('name ::: ' + this.name);
  }
  onClic() {
    console.log('onClic');
  }
  title = 'FormationEd1 NEW';
  isDisabled = false;

  name = 'Kadio';
  currentRoute = 'home';

  goToHome() {
    this.currentRoute = 'home';
    this.router.navigateByUrl('/home');
  }
  goToUser() {
    this.currentRoute = 'users';

    this.router.navigateByUrl('/users');
  }
}
