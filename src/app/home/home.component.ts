import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiBackendService } from '../services/api-backend.service';
import { environment } from '../../environments/environment.development';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
})
export class HomeComponent implements OnInit, OnDestroy {
  currentUserToShow: any;
  paginateResponse: any;
  defaultSize = 2;
  arrayPaginate: any = [];
  onclick() {
    this.router.navigateByUrl('users');
  }
  constructor(
    private http: HttpClient,
    private router: Router,
    private apiBackend: ApiBackendService
  ) {}
  ngOnDestroy(): void {}

  maj(id: any) {
    this.router.navigateByUrl('users/' + id);
  }

  voir1(id: any) {
    this.router.navigateByUrl('show-users/' + id);
  }
  voir2(data: any) {
    this.currentUserToShow = data;
  }

  paginate(index: any) {
    console.log('paginate');
    this.apiBackend
      .getWithPaginate(environment.endpointUser, index, this.defaultSize)
      .subscribe((value) => {
        this.paginateResponse = value;
        let localArrayPaginate = [];
        for (let i = 0; i < this.paginateResponse.pages; i++) {
          localArrayPaginate.push(i + 1);
        }
        this.arrayPaginate = localArrayPaginate;
      });
  }
  users: any;
  ngOnInit(): void {
    // this.apiBackend
    //   .getAll(environment.endpointUser)
    //   .subscribe((value) => (this.users = value));
    this.apiBackend
      .getWithPaginate(environment.endpointUser, 1, this.defaultSize)
      .subscribe((value) => {
        this.paginateResponse = value;

        for (let index = 0; index < this.paginateResponse.pages; index++) {
          this.arrayPaginate.push(index + 1);
        }
      });
  }
}
